<?xml version="1.0" encoding="utf-8"?>

<!-- ======================================================================= -->
<!-- Coalevo Bundle build file                                               -->
<!-- ======================================================================= -->
<project name="Coalevo-Webresource-Presence" default="compile" basedir=".">

  <!-- Allow any user specific values to override the defaults -->
  <property file="${user.home}/.build.properties"/>
  <property file="./build.properties"/>

  <target name="splash" depends="" if="build.visual"
          description="Displays a splash with a build status.">
    <splash imageurl="http://www.coalevo.net/images/logo_coalevo.png"
            useproxy="true"/>
  </target>

  <target name="init" depends="splash">
    <tstamp>
      <format property="src.timestamp" pattern="dd/MM/yyyy"/>
      <format property="year" pattern="2004-yyyy"/>
    </tstamp>

    <!-- Load License Header -->
    <loadfile property="license"
              srcFile="LICENSE.txt"
              encoding="UTF-8"
        >
      <filterchain>
        <replacetokens>
          <token key="PROJECTNAME" value="${bundle.name}"/>
        </replacetokens>
      </filterchain>
    </loadfile>

    <property name="Name" value="${bundle.name}"/>
    <property name="name" value="${project.sn}"/>
    <property name="version" value="${bundle.version}"/>
    <property name="copyright" value="${year} Coalevo (http://www.coalevo.net)"/>

    <!-- Filters -->
    <filter token="date" value="${src.timestamp}"/>
    <filter token="version" value="${version}"/>
    <filter token="copyright" value="${copyright}"/>
    <filter token="license" value="${license}"/>

    <!-- Source related properties -->
    <property name="src.dir" value="${basedir}${file.separator}src"/>
    <property name="src.java" value="${src.dir}${file.separator}java"/>
    <property name="src.docs" value="${src.dir}${file.separator}documentation"/>

    <property name="lib.dir" value="${basedir}${file.separator}lib${file.separator}"/>
    <property name="src.excludes" value=""/>

    <!-- Build related properties -->
    <property name="build.dir"
              value="${basedir}${file.separator}..${file.separator}build${file.separator}${project.sn}"/>
    <property name="build.dist" value="${build.dir}${file.separator}dist"/>
    <property name="build.dist.src" value="${build.dist}${file.separator}${project.sn}-${version}-src"/>
    <property name="build.dist.bin" value="${build.dist}${file.separator}${project.sn}-${version}-bin"/>
    <property name="build.dist.src.pkg" value="${project.sn}-${version}-src"/>
    <property name="build.dist.bin.pkg" value="${project.sn}-${version}-bin"/>

    <property name="build.src" value="${build.dir}${file.separator}src"/>
    <property name="build.classes" value="${build.dir}${file.separator}classes"/>
    <property name="build.docs" value="${build.dir}${file.separator}docs"/>
    <property name="build.javadocs" value="${build.docs}${file.separator}api"/>
    <property name="build.jar" value="${build.dir}${file.separator}${name}.jar"/>
    <property name="build.jar.api" value="${build.dir}${file.separator}${name}-api.jar"/>
    <path id="build.classpath">
      <fileset dir="${lib.dir}">
        <include name="**/*.jar"/>
      </fileset>
      <pathelement location="${basedir}/../build/coalevo-logging/coalevo-logging-api.jar"/>
      <pathelement location="${basedir}/../build/coalevo-foundation/coalevo-foundation-api.jar"/>
      <pathelement location="${basedir}/../build/coalevo-security/coalevo-security-api.jar"/>
      <pathelement location="${basedir}/../build/coalevo-system-services/coalevo-system-services-api.jar"/>
      <pathelement location="${basedir}/../build/coalevo-presence/coalevo-presence-api.jar"/>
      <pathelement location="${basedir}/../build/coalevo-statistics/coalevo-statistics-api.jar"/>
      <pathelement location="${basedir}/../build/coalevo-webresource/coalevo-webresource-api.jar"/>
    </path>
    <property name="build.metatype" value="${build.classes}/OSGI-INF/metatype"/>
    <property name="build.l10n" value="${build.classes}/OSGI-INF/l10n"/>

    <property name="build.dist" value="${build.dir}${file.separator}${name}-${version}"/>

    <property name="dist.basedir" value="${build.dir}"/>
    <property name="dist.includes" value="${name}${file.separator}**"/>
    <property name="dist.excludes"
              value="**/prj/**,
            **/dist/**,
             **/test/**,
             **/model/**,
             **/build/**,
             **/.DS_Store"
        />
    <property name="packages" value="${build.packages}"/>
    <property name="library.includes" value="${bundle.includes}"/>
    <property name="api.includes" value="${bundle.api.includes}"/>

  </target>

  <!-- =================================================================== -->
  <!-- Basic build targets                                                 -->
  <!-- =================================================================== -->
  <target name="prepare-sources" depends="init">
    <mkdir dir="${build.dir}"/>
    <mkdir dir="${build.src}"/>

    <copy todir="${build.src}" filtering="true">
      <fileset dir="${src.java}" excludes="${src.excludes}"/>
    </copy>
  </target>

  <!-- =================================================================== -->
  <!-- Compiles the source code                                            -->
  <!-- =================================================================== -->
  <target name="compile"
          depends="prepare-sources"
          description="Compiles the sources. (Default)">
    <!-- Build number -->
    <buildnumber/>
    <!-- Echo compilation properties -->
    <echo level="info" message="Build Number = ${build.number}"/>
    <echo level="info" message="Compiler     = ${build.compiler}"/>
    <echo level="info" message="Optimize     = ${compile.optimize}"/>
    <echo level="info" message="Deprecation  = ${compile.deprecation}"/>
    <echo level="info" message="Debug        = ${compile.debug}"/>

    <mkdir dir="${build.classes}"/>
    <javac srcdir="${build.src}"
           destdir="${build.classes}"
           debug="${compile.debug}"
           optimize="${compile.optimize}"
           deprecation="${compile.deprecation}"
        >
      <classpath refid="build.classpath"/>
      <compilerarg line="${build.compilerargs}"/>
    </javac>
    <!-- Copy resource files -->
    <copy todir="${build.classes}">
      <fileset dir="${build.src}" casesensitive="yes">
        <include name="**/*.xml"/>
        <include name="**/*.properties"/>
        <include name="**/*.png"/>
        <include name="**/*.gif"/>
        <include name="**/*.jpg"/>
        <include name="**/*.html"/>
        <include name="**/*.js"/>
        <include name="**/*.css"/>
        <include name="**/*.swf"/>
        <include name="**/*.mp3"/>
        <exclude name="**/.DS_Store"/>
        <exclude name="**/resources/metatype/*"/>
        <exclude name="**/resources/l10n/*"/>
      </fileset>
    </copy>

    <!-- Copy special bundle resources -->
    <available filepath="${build.src}" file="${bundle.resources.l10n}" type="dir"
               property="copy.l10n"/>
    <antcall target="copy-l10n"/>
    <available filepath="${build.src}" file="${bundle.resources.metatype}" type="dir"
               property="copy.metatype"/>
    <antcall target="copy-metatype"/>

  </target>

  <target name="copy-l10n" depends="" if="copy.l10n">
    <mkdir dir="${build.l10n}"/>
    <copy todir="${build.l10n}">
      <fileset dir="${build.src}/${bundle.resources.l10n}" casesensitive="yes">
        <exclude name="**/.DS_Store"/>
      </fileset>
    </copy>

  </target>

  <target name="copy-metatype" depends="" if="copy.metatype">
    <mkdir dir="${build.metatype}"/>
    <copy todir="${build.metatype}">
      <fileset dir="${build.src}/${bundle.resources.metatype}" casesensitive="yes">
        <exclude name="**/.DS_Store"/>
      </fileset>
    </copy>
  </target>

  <!-- =================================================================== -->
  <!-- Creates the jar archive                                             -->
  <!-- =================================================================== -->
  <target name="jar" depends="compile" description="Builds the library (.jar).">
    <jar jarfile="${build.jar}"
         basedir="${build.classes}"
         includes="${library.includes},OSGI-INF/**"
         index="true">
      <metainf dir="${basedir}" includes="LICENSE.txt,NOTICE.txt" />
      <manifest>
        <attribute name="Built-By" value="${user.name}"/>
        <attribute name="Build-Number" value="${build.number}"/>
        <attribute name="Bundle-ManifestVersion" value="2"/>
        <attribute name="Bundle-RequiredExecutionEnvironment" value="${bundle.executionenvironment}"/>
        <attribute name="Bundle-Name" value="${bundle.name}"/>
        <attribute name="Bundle-SymbolicName" value="${bundle.sn}"/>
        <attribute name="Bundle-Version" value="${version}"/>
        <attribute name="Bundle-Description" value="${bundle.description}"/>
        <attribute name="Bundle-Activator" value="${bundle.activator}"/>
        <attribute name="Bundle-Category" value="${bundle.category}"/>
        <attribute name="Bundle-Classpath" value="${bundle.classpath}"/>
        <attribute name="Bundle-ContactAddress" value="${bundle.contact}"/>
        <attribute name="Bundle-Vendor" value="${bundle.vendor}"/>
        <attribute name="Bundle-Copyright" value="${copyright}"/>
        <attribute name="Bundle-DocUrl" value="${bundle.docurl}"/>
        <attribute name="Import-Package" value="${bundle.imports}"/>
        <attribute name="Export-Package" value="${bundle.exports}"/>
      </manifest>
    </jar>
    <!-- If Required embed jars -->
    <antcall target="embed-jars"/>
  </target>

  <target name="embed-jars" if="bundle.embed-jars">
    <jar jarfile="${build.jar}"
         update="true"
         basedir="${basedir}"
         includes="${bundle.embed-jars}"
         index="true"/>
  </target>

  <target name="api-jar" depends="compile" description="Builds the API Jar for the compile time dependency.">
    <jar jarfile="${build.jar.api}"
         basedir="${build.classes}"
         includes="${api.includes}"
        />
  </target>

  <target name="rebuild" depends="clean-build,jar,api-jar"/>  

  <!-- =================================================================== -->
  <!-- Creates the API documentation                                       -->
  <!-- =================================================================== -->
  <target name="javadocs" depends="prepare-sources">
    <mkdir dir="${build.javadocs}"/>
    <javadoc packagenames="${packages}"
             sourcepath="${build.src}"
             destdir="${build.javadocs}"
             author="true"
             version="true"
             windowtitle="${Name} API"
             doctitle="${Name} API Documentation"
             header="&lt;a href=&apos;http://www.coalevo.net&apos; target=&apos;_top&apos;&gt;&lt;img src=&apos;http://www.coalevo.net/logo_coalevo_api.png&apos; alt=&apos;Coalevo Logo&apos; border=&apos;0&apos; hspace=&apos;1&apos; vspace=&apos;1&apos;&gt;&lt;/a&gt;"
             bottom="Copyright &#169; ${copyright}"
             additionalparam="${javadocs.additionalparam}"
        >
      <classpath refid="build.classpath"/>
    </javadoc>
  </target>

  <!-- =================================================================== -->
  <!-- Source Distribution Targets                                         -->
  <!-- =================================================================== -->
  <target name="prepare-srcdist" depends="prepare-sources"
          description="Prepares everything required for the source distribution packages.">
    <property name="srcdist.src" value="${build.dist.src}${file.separator}src"/>
    <property name="srcdist.docs" value="${srcdist.src}${file.separator}documentation"/>
    <property name="srcdist.java" value="${srcdist.src}${file.separator}java"/>
    <property name="srcdist.lib" value="${build.dist.src}${file.separator}lib"/>

    <mkdir dir="${build.dist}"/>
    <mkdir dir="${build.dist.src}"/>
    <mkdir dir="${srcdist.src}"/>
    <mkdir dir="${srcdist.lib}"/>
    <mkdir dir="${srcdist.java}"/>
    <mkdir dir="${srcdist.docs}"/>

    <!-- Copy sources -->
    <copy todir="${srcdist.java}" includeEmptyDirs="false">
      <fileset dir="${build.src}"/>
    </copy>

    <!-- Copy documentation source -->
    <copy todir="${srcdist.docs}" filtering="true" includeEmptyDirs="false">
      <fileset dir="${src.docs}"/>
    </copy>

    <!-- Copy libraries -->
    <copy todir="${srcdist.lib}">
      <fileset dir="${lib.dir}" excludes=""/>
    </copy>

    <!-- Copy Readme, License and Status -->
    <!-- <copy file="${basedir}${file.separator}README.txt" todir="${build.srcdist}"/>-->
    <copy file="${basedir}${file.separator}LICENSE.txt" todir="${build.dist.src}"/>
    <copy file="${basedir}${file.separator}status.xml" todir="${build.dist.src}"/>

    <!-- Copy build scripts -->
    <copy file="${basedir}${file.separator}build.xml" todir="${build.dist.src}"/>
    <copy file="${basedir}${file.separator}build.properties" todir="${build.dist.src}"/>
    <copy file="${basedir}${file.separator}forrest.properties" todir="${build.dist.src}"/>
  </target>

  <target name="srcdist" depends="prepare-srcdist" description="Builds a tar-gzip source distribution package.">
    <tar tarfile="${build.dist}${file.separator}${build.dist.src.pkg}.tgz"
         compression="gzip"
         basedir="${build.dist}"
         includes="${build.dist.src.pkg}/**"
        />
  </target>

  <target name="srczipdist" depends="prepare-srcdist" description="Builds a zipped source distribution file.">
    <zip zipfile="${build.dist}${file.separator}${build.dist.src.pkg}.zip"
         basedir="${build.dist}"
         includes="${build.dist.src.pkg}/**"
        />
  </target>

  <!-- =================================================================== -->
  <!-- Binary Distribution Targets                                         -->
  <!-- =================================================================== -->
  <target name="prepare-bindist" depends="jar, javadocs"
          description="Prepares everything required for the binary distribution packages.">
    <property name="bindist.docs" value="${build.dist.bin}${file.separator}docs"/>

    <mkdir dir="${build.dist}"/>
    <mkdir dir="${build.dist.bin}"/>
    <mkdir dir="${bindist.docs}"/>

    <!-- Copy built documentation -->
    <copy todir="${bindist.docs}" filtering="true" includeEmptyDirs="false">
      <fileset dir="${build.docs}"/>
    </copy>

    <!-- Copy Readme and License -->
    <!-- <copy file="${basedir}${file.separator}README.txt" todir="${build.dist}"/> -->
    <copy file="${basedir}${file.separator}LICENSE.txt" todir="${build.dist.bin}"/>

    <!-- Copy build -->
    <copy file="${build.jar}" todir="${build.dist.bin}"/>
  </target>

  <target name="bindist" depends="prepare-bindist" description="Builds a tar-gzip binary distribution file.">
    <tar tarfile="${build.dist}${file.separator}${build.dist.bin.pkg}.tgz"
         compression="gzip"
         basedir="${build.dist}"
         includes="${build.dist.bin.pkg}/**"
        />
  </target>

  <target name="binzipdist" depends="prepare-bindist" description="Builds a zipped binary distribution file.">
    <zip zipfile="${build.dist}${file.separator}${build.dist.bin.pkg}.zip"
         basedir="${build.dist}"
         includes="${build.dist.bin.pkg}/**"
        />
  </target>

  <!-- =================================================================== -->
  <!-- Create docs distribution                                            -->
  <!-- =================================================================== -->
  <target name="docsdist" depends="javadocs">
    <zip zipfile="${build.dist}${file.separator}${name}${version}_docs.zip"
         basedir="${build.dir}"
         includes="docs/**"
        />
  </target>

  <!-- =================================================================== -->
  <!-- Release target                                                      -->
  <!-- =================================================================== -->
  <target name="release"
          depends="clean-build, srcdist, srczipdist, bindist, binzipdist, docsdist" description="Builds a release.">
    <echo level="info" message="Created release ${version}"/>
  </target>

  <!-- =================================================================== -->
  <!-- Deploy target                                                       -->
  <!-- =================================================================== -->
  <target name="deploy" depends="init">
    <copy todir="${deploy.dir}" file="${build.jar}"/>
  </target>

  <!-- =================================================================== -->
  <!-- Cleanup Targets                                                     -->
  <!-- =================================================================== -->
  <target name="clean-classes" depends="init" description="Cleans up the build classes.">
    <delete dir="${build.classes}"/>
  </target>

  <target name="clean-jars" depends="init" description="Cleans up the build libraries.">
    <delete file="${build.jar}"/>
  </target>

  <target name="clean-build" depends="init" description="Cleans up the build directory.">
    <delete dir="${build.dir}"/>
  </target>

  <target name="clean-dist" depends="init" description="Cleans up distribution builds.">
    <delete dir="${build.dist}"/>
  </target>

  <target name="clean-docs" depends="init" description="Cleans up generated javadocs.">
    <delete dir="${build.javadocs}"/>
  </target>

  <target name="clean-all" depends="clean-build,clean-dist, clean-docs" description="Cleans up to distribution state.">
    <echo level="info" message="Cleaned all."/>
  </target>

</project>

