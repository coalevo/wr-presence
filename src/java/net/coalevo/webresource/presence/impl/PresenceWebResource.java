/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.presence.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.presence.model.PresenceFilter;
import net.coalevo.presence.model.PresenceProxy;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.webresource.model.BaseWebResource;
import net.coalevo.webresource.model.WebResource;
import net.coalevo.webresource.model.WebResourceException;
import net.coalevo.webresource.model.WebResourceRequest;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

/**
 * Provides a {@link WebResource} implementation that will provide
 * public presence data (aka advertised available).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceWebResource
    extends BaseWebResource {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(PresenceWebResource.class.getName());
  private ServiceMediator m_Services = Activator.getServices();

  public PresenceWebResource() {
    super("presence");
  }//constructor

  public void handle(WebResourceRequest resreq)
      throws WebResourceException {

    //1. prepare resources
    PresenceService ps = m_Services.getPresenceService(ServiceMediator.NO_WAIT);
    ServiceAgent sag = resreq.getServiceAgent();

    //2. assert run
    if (ps == null || sag == null) {
      resreq.failInternalError();
      return;
    }
    PresenceFilter pf = PresenceFilter.NONE;
    if (resreq.hasSubResources()) {
      String agent = resreq.getSubResources()[0];
      Activator.log().debug(c_LogMarker, "subres:" + agent);
      AgentIdentifier aid = resreq.getAgentIdentifier(agent);
      Activator.log().debug(c_LogMarker, "subres:" + aid.getIdentifier());
      pf = new AgentPresenceFilter(aid);
    }
    Iterator iter = ps.listAdvertisedPresent(sag, pf);
    if (!iter.hasNext()) {
      resreq.failNoContent();
      return;
    }
    try {
      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        resreq.setJSONResponseType();

        PrintWriter pw = resreq.getOutputWriter();
        pw.print(LIST_START_JSON);
        while (iter.hasNext()) {
          AgentIdentifier aid = (AgentIdentifier) iter.next();
          PresenceProxy p = ps.getAdvertisedPresent(sag, aid);
          try {
            if (p != null) {
              String nick = resreq.resolveNickname(aid.toString());
              String since = formatUTCDateTime(p.presentSince());
              pw.print(String.format(PRESENCE_TEMPLATE_JSON,
                  toJSONString(aid.getIdentifier(), false),
                  toJSONString(nick, false),
                  toJSONString(since, false),
                  toJSONString(p.getStatusDescription(), false)
              ));
            }
          } catch (Exception ex) {
          }
        }

        pw.print(LIST_END_JSON);
        pw.close();
      } else {
        resreq.setXMLResponseType();
        PrintWriter pw = resreq.getOutputWriter();
        pw.println(XML_PREFIX);
        //Note: Client transformation does not really work properly
        //pw.println(STYLESHEET_INSTRUCTION);
        pw.print(LIST_START_XML);
        while (iter.hasNext()) {
          AgentIdentifier aid = (AgentIdentifier) iter.next();
          PresenceProxy p = ps.getAdvertisedPresent(sag, aid);
          try {
            if (p != null) {
              String nick = resreq.resolveNickname(aid.toString());
              String since = formatUTCDateTime(p.presentSince());
              pw.print(String.format(PRESENCE_TEMPLATE_XML,
                  aid.getIdentifier(),
                  nick,
                  since,
                  p.getStatusDescription()
              ));
            }
          } catch (Exception ex) {
          }
        }
        pw.print(LIST_END_XML);
        pw.close();
      }
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "handle()", ex);
    }
    return;
  }//handle

  class AgentPresenceFilter
      implements PresenceFilter {

    private AgentIdentifier m_AgentIdentifier;

    public AgentPresenceFilter(AgentIdentifier aid) {
      m_AgentIdentifier = aid;
    }//constructor

    public boolean allows(PresenceProxy p) {
      return m_AgentIdentifier.equals(p.getAgentIdentifier());
    }//allows

  }//AgentPresenceFilter

  //public static final String  STYLESHEET_INSTRUCTION = "<?xml-stylesheet  type=\"text/xsl\" href=\"http://www.coalevo.net/xsl/presencelist-xhtml.xsl\"?>";
  private static final String PARAM_FORMAT = "format";

  private static final String LIST_START_XML = "<presencelist xmlns=\"http://www.coalevo.net/schemas/presence-list-v10\" version=\"1.0\">";
  private static final String PRESENCE_TEMPLATE_XML = " <presence aid=\"%s\" nickname=\"%s\" since=\"%s\"><![CDATA[%s]]></presence>";
  private static final String LIST_END_XML = "</presencelist>";

  private static final String LIST_START_JSON = "{\"presences\":[";
  private static final String PRESENCE_TEMPLATE_JSON = "{\"aid\":\"%s\",\"nick\":\"%s\",\"since\":\"%s\",\"sd\":\"%s\"}";
  private static final String LIST_END_JSON = "]}";

}//class PresenceWebResource
