/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.presence.impl;

import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.logging.service.LoggingService;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.statistics.service.StatisticsService;
import net.coalevo.system.service.ExecutionService;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private Marker m_LogMarker = MarkerFactory.getMarker(ServiceMediator.class.getName());
  private BundleContext m_BundleContext;

  private MessageResourceService m_MessageResourceService;
  private ExecutionService m_ExecutionService;
  private PresenceService m_PresenceService;
  private StatisticsService m_StatisticsService;

  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_ExecutionServiceLatch;
  private CountDownLatch m_PresenceServiceLatch;
  private CountDownLatch m_StatisticsServiceLatch;

  private ConfigurationMediator m_ConfigurationMediator;

  public ServiceMediator() {

  }//constructor

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getMessageResourceService()", e);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public PresenceService getPresenceService(long wait) {
    try {
      if (wait < 0) {
        m_PresenceServiceLatch.await();
      } else if (wait > 0) {
        m_PresenceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getPresenceService()", e);
    }
    return m_PresenceService;
  }//getPresenceService

  public ExecutionService getExecutionService(long wait) {
    try {
      if (wait < 0) {
        m_ExecutionServiceLatch.await();
      } else if (wait > 0) {
        m_ExecutionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      Activator.log().error(m_LogMarker, "getExecutionService()", e);
    }
    return m_ExecutionService;
  }//getExecutionService

  public StatisticsService getStatisticsService(long wait) {
    try {
      if (wait < 0) {
        m_StatisticsServiceLatch.await();
      } else if (wait > 0) {
        m_StatisticsServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_StatisticsService;
  }//getStatisticsService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigurationMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigurationMediator = configMediator;
  }//setConfigMediator

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_MessageResourceServiceLatch = createWaitLatch();
    m_ExecutionServiceLatch = createWaitLatch();
    m_PresenceServiceLatch = createWaitLatch();
    m_StatisticsServiceLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(objectclass=" + MessageResourceService.class.getName() + ")" +
            "(objectclass=" + ExecutionService.class.getName() + "))" +
            "(objectclass=" + PresenceService.class.getName() + "))" +
            "(objectclass=" + StatisticsService.class.getName() + "))" +
            "(objectclass=" + LoggingService.class.getName() + "))";


    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_MessageResourceService = null;
    m_PresenceService = null;
    m_ExecutionService = null;
    m_ConfigurationMediator = null;
    m_StatisticsService = null;

    //Latches
    if (m_MessageResourceServiceLatch != null) {
      m_MessageResourceServiceLatch.countDown();
      m_MessageResourceServiceLatch = null;
    }
    if (m_ExecutionServiceLatch != null) {
      m_ExecutionServiceLatch.countDown();
      m_ExecutionServiceLatch = null;
    }
    if (m_PresenceServiceLatch != null) {
      m_PresenceServiceLatch.countDown();
      m_PresenceServiceLatch = null;
    }
    if (m_StatisticsServiceLatch != null) {
      m_StatisticsServiceLatch.countDown();
      m_StatisticsServiceLatch = null;
    }
    m_LogMarker = null;
    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      //System.err.println(ev.toString());
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            m_ExecutionServiceLatch.countDown();
          } else if (o instanceof PresenceService) {
            m_PresenceService = (PresenceService) o;
            m_PresenceServiceLatch.countDown();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = (StatisticsService) o;
            m_StatisticsServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            m_ExecutionServiceLatch = createWaitLatch();
          } else if (o instanceof PresenceService) {
            m_PresenceService = null;
            m_PresenceServiceLatch = createWaitLatch();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = null;
            m_StatisticsServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
